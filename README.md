# AWS re:Invent GameDay repo

This project uses Hashicorp's Terraform to:

1) Build a small GitLab Enterprise Edition (EE) instance on AWS using the official GitLab EE AMI

2) Use the GitLab Terraform provider to add sample data

# What is GitLab?

[GitLab](https://about.gitlab.com/) is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate and build software. From idea to production, GitLab helps teams improve cycle time from weeks to minutes, reduce development costs and time to market while increasing developer productivity.

# How can I install GitLab?

GitLab can be consumed as a [SaaS](http://gitlab.com/) or [installed on a number of platforms](https://about.gitlab.com/install/). All that is required for the GitLab server is a Linux OS. For the GameDay event, we will use Terraform to provision at GitLab instance published by the GitLab fulfillment team.  

## Getting Started

* Ensure the AWS security keys, region & session ID have been exported as shown in the GameDay interface
* Ensure you have placed the private key in the GameDay interface in ~/.ssh/ & have the proper permissions to protect the key.
* Terraform > 0.12. If you are using an older version of Terraform then you WILL receive an error.


## First Terraform Run - Create a GitLab instance on AWS

A list of what is deployed:

* Latest version of GitLab Enterprise Edition Open Core on AWS using EC2.
* Security groups to allow ICMP/ping, HTTP (TCP/80), HTTPS (TCP/443) & SSH (TCP/22) access on the EC2 instance from the world.


### Setup GitLab:

Ensure Hashicorp Terraform is installed & is > 0.12

``terraform -v``

If terraform is not installed, or is not a recent build, then grab the correct binary for your OS on the [terraform site](https://www.terraform.io/downloads.html)

After confirming Terraform is installed locally, clone the sample project:

``git clone https://gitlab.com/khair1/aws-gameday-reinvent-2019``

Once cloned, change to the newly cloned project:

``cd aws-gameday-reinvent-2019``

The project contains two main directories:
* environments/default
* gitlab-data

The first, environments/default contains the files needed to standup the GitLab sever. So.. let's start building!

``cd environments/default``

Next, let's initialize terraform and download the required modules:

``terraform init``

Now that we have downloaded the AWS & random provider, let's have terraform read all the .tf files in the directory & create a plan:

``terraform plan -out gl.tfplan``

For the final step, instruct terraform to deploy the infrastructure created in the previous plan command:

``terraform apply -input=false gl.tfplan``

Once Terraform completes the run, you'll see the public server IP address on the bottom line:

![Terraform Output](graphics/outputs-first-run.png)

Open a browser to view the newly created server using the "Public-IP-Address-GitLabEE"

Please be patient as the server is using a T2.Medium instance. You will receive various errors & warnings (mostly of a 502 variety) until the server has spun up properly.

When you see the change password screen, the GitLab instance is ready:

![Change GitLab EE root password](graphics/password-changeme-root.png)


------------

Deeper dive 1 (post lab)

For the GameDay environment, we are using simple HTTP / non-encrypted for the lab. GitLab highly recommends that TLS be enabled. GitLab uses [Let's Encrypt](https://letsencrypt.org/) for TLS in the event that you do not have a certificate. This is done simply by updating the server configuration. An example script can be found in the project under the [scripts directory](environments/default/scripts/provision2.sh)

By default, a DNS record is required for Let's Encrypt to validate  ownership.

------------

Deeper dive 2 (post lab)

The Terraform commands above can be used in a GitLab CI/CD pipeline. An example can be found in a different project. If you're interested in checking out how to do this then see this [project's CI configuration](https://gitlab.com/khair1/gitlab-with-gitlab-via-terraform-on-aws/blob/master/.gitlab-ci.yml).


## Second Terraform Run - Add data to a GitLab server

### Seed GitLab:

For the 2nd run, we will use the Terraform GitLab provider to add data to the GitLab server.

In order to interact with the GitLab server programmatically, you will need a token. To grab what GitLab calls as Personal Access Token (PAT), you will need to set a root password & then log in to the GitLab server.

To create a PAT, you will need to login to the GitLab instance. To login, change the root password of the server (last step of the first Terraform run section above) & browse to http://<IP Address>/profile/personal_access_tokens

Create a token name to your liking & check all the boxes before clicking on the "create personal access token" button like shown:

![Create root PAT](graphics/pat-creation.png)

``IMPORTANT!`` The token that is generated will not be shown again. Please ensure you have either copied it to a text file or written it down. The token can be easily copied by clicking on the two box icon next to the "Your New Personal Access Token" text box.

Let's return to our cloned project. We will run Terraform again. This time, however, we'll use the GitLab provider to add the sample data found in gitlab-data/gitlab-data-add.tf

``cd ../../gitlab-data``

Now, let's initialize terraform and download the GitLab provider:

``terraform init``

Before running the Terraform plan, you will need to decide how to pass variables This can be accomplished by either updating the [Variables.tf](https://gitlab.com/khair1/aws-gameday-reinvent-2019/gitlab-data/Variables.tf) with your token ID & URL -or- manually inputing these when prompted -or- pass the variables during the plan phase.

Option 1 - Using edited Variables.tf path:

``terraform plan -out gl.tfplan``

Option 2 - Using prompted path:

``terraform plan -out gl.tfplan``

![Seed GitLab Data](graphics/gitlab-seeding.png)

Option 3 - Passing variables in the plan command:

``terraform plan -out gl.tfplan -var 'gitlab_token=<token ID>' -var 'gitlab_url=http://<IP/FQDN>'``


The first option is preferred as it's easier to create & destroy data without the need to specify the token & URL. We'll teardown the site in the next section. For now, let's create the seed data:

``terraform apply -input=false gl.tfplan``

## Teardown of GitLab server

Teardown is in reverse order if you choose not to destroy the instance itself:

In the gitlab-data directory:

If the Variables.tf was updated then we can remove the seeded data via a simple terraform destroy:

``terraform destroy -auto-approve``

If the the PAT & URL will specified, or passed, via the command line then you will need to specify these variables:

``terraform destroy -auto-approve -var 'gitlab_token=<token ID>' -var 'gitlab_url=http://<IP/FQDN>'``

Next, in the environments/default directory:

``terraform destroy -auto-approve``
