data "aws_ami" "GitLab-EE" {
  most_recent = true

  filter {
    name   = "name"
    # values = ["GitLab EE ${var.gitlab_version}"]
    values = ["GitLab EE *"]
  }

  owners = ["782774275127"]
}

resource "aws_instance" "GitLabEE" {
  ami = "${data.aws_ami.GitLab-EE.id}"
  # ami = "ami-0aa817308d50abb76"
  instance_type = "${var.GitLabEE_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.HTTP_HTTPS_SSH.id}",
    "${aws_security_group.Allow_ICMP_GL.id}"
  ]
  # key_name = "${var.private_ssh_key_name}"
  key_name = var.ssh_key
  # provisioner "file" {
  #     connection {
  #       type     = "ssh"
  #       user     = "ubuntu"
  #       host     = "${aws_instance.GitLabEE.public_ip}"
  #       private_key  = file("${var.private_ssh_key}")
  #     }
  #     source      = "scripts/provision.sh"
  #     destination = "/tmp/provision.sh"
  # }
  #
  # provisioner "remote-exec" {
  #   connection {
  #    type     = "ssh"
  #    user     = "ubuntu"
  #    host     = "${aws_instance.GitLabEE.public_ip}"
  #    private_key  = file("${var.private_ssh_key}")
  #   }
  #   inline = [
  #     "chmod +x /tmp/*.sh",
  #     "sudo /tmp/provision.sh",
  #   ]
  # }

  # provisioner "file" {
  #     connection {
  #       type     = "ssh"
  #       user     = "ubuntu"
  #       host     = "${aws_instance.GitLabEE.public_ip}"
  #       private_key  = file("${var.private_ssh_key}")
  #     }
  #     source      = "scripts/provision2.sh"
  #     destination = "/tmp/provision2.sh"
  # }
  # provisioner "remote-exec" {
  #   connection {
  #    type     = "ssh"
  #    user     = "ubuntu"
  #    host     = "${aws_instance.GitLabEE.public_ip}"
  #    private_key  = file("${var.private_ssh_key}")
  #   }
  #   inline = [
  #     "sudo at -f /tmp/provision2.sh now + 1 minute",
  #   ]
  # }
  tags = {
    Name = "GitLab_EE-AWS-gameday"
  }

}
