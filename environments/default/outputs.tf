

output "Public-IP-Address-GitLabEE" {
  value = "${aws_instance.GitLabEE.public_ip}"
}

output "Private-IP-Address-GitLabEE" {
  value = "${aws_instance.GitLabEE.private_ip}"
}
