variable "region" {
  default     = "us-east-1"
  description = "The name of the AWS region you'd like to deploy in."
}

data "aws_vpc" "default" {
  default = true
}

variable "GitLabEE_EC2_instance_type" {
  # default = "t2.medium"
  default = "t2.large"
  # May want to use a larger instance such as:
  # default = "m5ad.2xlarge"
}

variable "SG-Prefix" {
  default     = "AWS-GameDay-"
  description = "Prefix to add to security groups"
}

variable "ssh_key" {
  default     = "ee-default-keypair"
  #default     = "cs-demo"
  description = "This is the name of your provisioning machine's public SSH key"
}
