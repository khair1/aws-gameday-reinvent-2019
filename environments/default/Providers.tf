provider "aws" {
  version = ">=  2.22"
}

provider "random" {
  version = "~> 2.1"
}
