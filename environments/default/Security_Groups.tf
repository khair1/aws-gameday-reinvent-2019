resource "random_string" "sg_prefix" {
  length           = 7
  upper            = false
  lower            = true
  number           = true
  special          = false
  override_special = "/@\" "
}

resource "aws_security_group" "HTTP_HTTPS_SSH" {
  name = "${var.SG-Prefix}${random_string.sg_prefix.result}-http_https_ssh"
  description = "HTTP, HTTPS & SSH CONNECTIONS INBOUND (managed by Terraform)"
  vpc_id = "${data.aws_vpc.default.id}"

  ingress {
         from_port = 443
         to_port = 443
         protocol = "TCP"
         cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
       from_port = 80
       to_port = 80
       protocol = "TCP"
       cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "Allow_ICMP_GL" {
  name = "${var.SG-Prefix}${random_string.sg_prefix.result}-allow_ICMP_GL"
  description = "Allow ICMP connections (managed by Terraform)"
  vpc_id = "${data.aws_vpc.default.id}"


ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
  }
egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}
