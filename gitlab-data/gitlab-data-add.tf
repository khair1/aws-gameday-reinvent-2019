# Configure the GitLab Provider
provider "gitlab" {
    token = "${var.gitlab_token}"
    base_url = "${var.gitlab_url}/api/v4"
}

resource "gitlab_user" "dev1" {
  name             = "Developer One"
  username         = "dev1"
  password         = "password12"
  email            = "dev1@{{gitlab_url}}"
  is_admin         = false
  can_create_group = true
  is_external      = false
}

resource "gitlab_user" "dev2" {
  name             = "Developer Two"
  username         = "dev2"
  password         = "password12"
  email            = "dev2@{{gitlab_url}}"
  is_admin         = false
  can_create_group = true
  is_external      = false
}

# resource "gitlab_user" "maint1" {
#   name             = "Maintainer 1"
#   username         = "maint1"
#   password         = "password12"
#   email            = "maint1@${var.gitlab_url}"
#   is_admin         = false
#   can_create_group = true
#   is_external      = false
# }
#
# resource "gitlab_user" "GodMode" {
#   name             = "God Mode"
#   username         = "godmode"
#   password         = "password12"
#   email            = "godmode@${var.gitlab_url}"
#   is_admin         = true
#   can_create_group = true
#   is_external      = false
# }
#
# resource "gitlab_user" "contractor1" {
#   name             = "Jill Contractor"
#   username         = "jillc"
#   password         = "password12"
#   email            = "jillc@${var.gitlab_url}"
#   is_admin         = false
#   can_create_group = false
#   is_external      = true
# }


resource "gitlab_group" "gameday" {
  name        = "gameday"
  path        = "gameday"
  description = "AWS Re:Invent GameDay Group"
}

#
# resource "gitlab_project" "SAs_All" {
#   name        = "Solution Architects"
#   description = "Skills by Solution Architect"
#   namespace_id = "${gitlab_group.SAs.id}"
# }
#
# resource "gitlab_label" "Rally_Working_Knowledge" {
#   project     = "${gitlab_project.SAs_All.id}"
#   name        = "Rally_Working_Knowledge"
#   description = "Have general Rally knowledge"
#   color       = "#ffccaa"
# }
#
# resource "gitlab_label" "Rally_Experts" {
#   project     = "${gitlab_project.SAs_All.id}"
#   name        = "Rally_Experts"
#   description = "Rally - deep domain expertise"
#   color       = "#ffcc00"
# }
#
# resource "gitlab_group" "Plan_Demo" {
#   name        = "Plan Demo"
#   parent_id   = "${gitlab_group.Plan.id}"
#   path        = "PlanDemo"
#   description = "Plan Demo"
# }
#
# resource "gitlab_group" "Plan_Design" {
#   name        = "Plan Design"
#   parent_id   = "${gitlab_group.Plan.id}"
#   path        = "Plan_Design"
#   description = "Can Design/ Architect Plan"
# }
#
# resource "gitlab_group" "Vendors" {
#   name        = "Vendors"
#   parent_id   = "${gitlab_group.Skills.id}"
#   path        = "vendors"
#   description = "Vendors "
# }
#
# resource "gitlab_group" "Rally" {
#   name        = "Rally"
#   parent_id   = "${gitlab_group.Vendors.id}"
#   path        = "Rally"
#   description = "Rally knowledge"
# }
#
# resource "gitlab_group" "Rally_Working_Knowledge" {
#   name        = "Rally working knowledge"
#   parent_id   = "${gitlab_group.Rally.id}"
#   path        = "Rally_Working_Knowledge"
#   description = "Have a good understanding of Rally"
# }
#
# resource "gitlab_group" "Rally_Experts" {
#   name        = "Rally experts"
#   parent_id   = "${gitlab_group.Rally.id}"
#   path        = "Rally_Experts"
#   description = "Rally Experts"
# }
