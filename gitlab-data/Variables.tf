variable "gitlab_token" {
 #   default = "y1yc-eKnyVsPkgxcMo8w"
 description = "The Personal Access token with API access. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!."
 }
 #
variable "gitlab_url" {
 #   default = "http://3.4.5.6"
 description = "Full URL GitLab demo environment including either http://<IP/FQDN> or https://<IP/FQDN>. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!"
 }

# Alternatively, you can also add the variables to the terraform apply command.
# The command line variables with supercede the above.
# Example:
# terraform apply -var 'gitlab_token=ExNM5pRVzzzP2wRs9jix' -var 'gitlab_url=http://3.4.5.6'
